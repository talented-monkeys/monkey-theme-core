var gulpTask = function() {
	// Read about the gulp tasks on:
	// https://bitbucket.org/talented-monkeys/monkey-theme-core/src/[ENVIRONMENT]/build/gulp/core/
	// Replace [ENVIRONMENT] with: production, staging, development
	require('../../../content/themes/monkey-theme/build/gulp/gulpfile.cache.versions').registerTasks({	
		namespace: 'parent',
		packageJsonFile: '../../package.json',
		dist: './content/themes/monkey-theme/config/cache/version',
		notificationTimeOut: 5,
	});
};


var GulpTaskCacheVersionsParentTheme = function () {
    this.gulpTask = gulpTask;
};

module.exports = new GulpTaskCacheVersionsParentTheme();