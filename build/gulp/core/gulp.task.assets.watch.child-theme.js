var gulpTask = function() {
	// Read about the gulp tasks on:
	// https://bitbucket.org/talented-monkeys/monkey-theme-core/src/[ENVIRONMENT]/build/gulp/core/
	// Replace [ENVIRONMENT] with: production, staging, development
	require('../../../content/themes/monkey-theme/build/gulp/gulpfile.build.assets.watch').registerTasks({
		namespace: 'child',
		themeDir: './content/themes/monkey-theme-child',
		sourcesStyles: [
			'./content/themes/monkey-theme-child/assets/styles/**/*.s+(a|c)ss',
			'./content/themes/monkey-theme-child/(layout|modules)/**/*.s+(a|c)ss',
		],
		sourcesScripts: [
			'./content/themes/monkey-theme-child/assets/scripts/**/*.(js|ts|coffee)js',
			'./content/themes/monkey-theme-child/assets/scripts/**/*.(js|ts|coffee).php',
			'./content/themes/monkey-theme-child/(layout|modules)/**/*.(js|ts|coffee)js',
			'./content/themes/monkey-theme-child/(layout|modules)/**/*.(js|ts|coffee).php',
		],
		sourcesImages: [
			'./content/themes/monkey-theme-child/assets/images/**/*.(png|jpg|gif|svg)',
			'./content/themes/monkey-theme-child/(layout|modules)/**/*.(png|jpg|gif|svg)',
		],
		dist: './content/themes/monkey-theme-child/dist/',
	});
};


var GulpTaskAssetsWatchChildTheme = function () {
    this.gulpTask = gulpTask;
};

module.exports = new GulpTaskAssetsWatchChildTheme();
