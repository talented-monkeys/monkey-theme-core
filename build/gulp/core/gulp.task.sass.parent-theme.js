var gulpTask = function() {
	// Read about the gulp tasks on:
	// https://bitbucket.org/talented-monkeys/monkey-theme-core/src/[ENVIRONMENT]/build/gulp/core/
	// Replace [ENVIRONMENT] with: production, staging, development
	require('../../../content/themes/monkey-theme/build/gulp/gulpfile.build.sass').registerTasks({
		namespace: 'parent',
		source: [
			'./content/themes/monkey-theme/assets/styles/**/*.s+(a|c)ss'
		],
		sassLintSource:[
			'./content/themes/monkey-theme/assets/styles/**/*.s+(a|c)ss',
			'!./content/themes/monkey-theme/assets/styles/vendor/**/*.s+(a|c)ss'
		],
		dist: './content/themes/monkey-theme/dist/css',
		distTmp: './content/themes/monkey-theme/dist/.tmp/css',
		sassLintConfigFile: './config/tests/lint/sass.lint.yml',
		notificationTimeOut: 5,
		includePaths: [
			'./content/themes/monkey-theme'
		]
	});
};


var GulpTaskSassParentTheme = function () {
    this.gulpTask = gulpTask;
};

module.exports = new GulpTaskSassParentTheme();
