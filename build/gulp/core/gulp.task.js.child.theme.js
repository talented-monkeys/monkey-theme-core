var gulpTask = function(){
	// Read about the gulp tasks on:
	// https://bitbucket.org/talented-monkeys/monkey-theme-core/src/[ENVIRONMENT]/build/gulp/core/
	// Replace [ENVIRONMENT] with: production, staging, development
	require('../../../content/themes/monkey-theme/build/gulp/gulpfile.build.javascript').registerTasks({
		namespace: 'child',
		source: [
			'./content/themes/monkey-theme-child/assets/scripts'
		],
		dist: './content/themes/monkey-theme-child/dist/js',
		distTmp: './content/themes/monkey-theme-child/dist/.tmp',
		coffeeLintConfig: './config/tests/lint/coffee.lint.json',
		jsLintConfig: './config/tests/lint/js.lint.yml',
		tsLintConfig: './config/tests/lint/ts.lint.yml',
		notificationTimeOut: 5,
		includePaths: [
			'./content/themes/monkey-theme-child'
		]
	});
};


var GulpTaskJsChildTheme = function () {
    this.gulpTask = gulpTask;
};

module.exports = new GulpTaskJsChildTheme();
