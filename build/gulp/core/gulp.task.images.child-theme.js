var gulpTask = function(){
	// Read about the gulp tasks on:
	// https://bitbucket.org/talented-monkeys/monkey-theme-core/src/[ENVIRONMENT]/build/gulp/core/
	// Replace [ENVIRONMENT] with: production, staging, development
	require('../../../content/themes/monkey-theme/build/gulp/gulpfile.build.images').registerTasks({
		namespace: 'child',
		source: ['./content/themes/monkey-theme/assets/images'],
		dist: './content/themes/monkey-theme-child/dist/img',
		notificationTimeOut: 5,
	});
};


var GulpTaskImagesChildTheme = function () {
    this.gulpTask = gulpTask;
};

module.exports = new GulpTaskImagesChildTheme();
