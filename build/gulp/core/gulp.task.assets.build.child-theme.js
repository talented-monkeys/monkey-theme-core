var gulpTask = function() {
	// Read about the gulp tasks on:
	// https://bitbucket.org/talented-monkeys/monkey-theme-core/src/[ENVIRONMENT]/build/gulp/core/
	// Replace [ENVIRONMENT] with: production, staging, development
	require('../../../content/themes/monkey-theme/build/gulp/gulpfile.build').registerTasks({
		namespace: 'child'
	});
};


var GulpTaskAssetsBuildChildTheme = function () {
    this.gulpTask = gulpTask;
};

module.exports = new GulpTaskAssetsBuildChildTheme();
