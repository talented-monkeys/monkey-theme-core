var gulpTask = function(){
	// Read about the gulp tasks on:
	// https://bitbucket.org/talented-monkeys/monkey-theme-core/src/[ENVIRONMENT]/build/gulp/core/
	// Replace [ENVIRONMENT] with: production, staging, development
	require('../../../content/themes/monkey-theme/build/gulp/gulpfile.build.sass').registerTasks({
		namespace: 'child',
		source: [
			'./content/themes/monkey-theme-child/assets/styles/**/*.s+(a|c)ss'
		],
		sassLintSource:[
			'./content/themes/monkey-theme-child/assets/styles/**/*.s+(a|c)ss',
			'!./content/themes/monkey-theme-child/assets/styles/vendor/**/*.s+(a|c)ss'
		],
		dist: './content/themes/monkey-theme-child/dist/css',
		distTmp: './content/themes/monkey-theme-child/dist/.tmp/css',
		sassLintConfigFile: './config/tests/lint/sass.lint.yml',
		notificationTimeOut: 5,
		includePaths: [
			'./content/themes/monkey-theme-child'
		]
	});
};


var GulpTaskSassChildTheme = function () {
    this.gulpTask = gulpTask;
};

module.exports = new GulpTaskSassChildTheme();
