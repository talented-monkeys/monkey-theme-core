var gulpTask = function() {
	// Read about the gulp tasks on:
	// https://bitbucket.org/talented-monkeys/monkey-theme-core/src/[ENVIRONMENT]/build/gulp/core/
	// Replace [ENVIRONMENT] with: production, staging, development
	require('../../../content/themes/monkey-theme/build/gulp/gulpfile.build.assets.watch').registerTasks({
		namespace: 'parent',
		themeDir: './content/themes/monkey-theme',
		sourcesStyles: [
			'./content/themes/monkey-theme/assets/styles/**/*.s+(a|c)ss',
			'./content/themes/monkey-theme/(layout|modules)/**/*.s+(a|c)ss',
		],
		sourcesScripts: [
			'./content/themes/monkey-theme/assets/scripts/**/*.(js|ts|coffee)',
			'./content/themes/monkey-theme/assets/scripts/**/*.(js|ts|coffee).php',
			'./content/themes/monkey-theme/(layout|modules)/**/*.(js|ts|coffee)',
			'./content/themes/monkey-theme/(layout|modules)/**/*.(js|ts|coffee).php',
		],
		sourcesImages: [
			'./content/themes/monkey-theme/assets/images/**/*.(png|jpg|gif|svg)',
			'./content/themes/monkey-theme/(layout|modules)/**/*.(png|jpg|gif|svg)',
		],
		dist: './content/themes/monkey-theme/dist/',
	});
};


var GulpTaskAssetsWatchParentTheme = function () {
    this.gulpTask = gulpTask;
};

module.exports = new GulpTaskAssetsWatchParentTheme();
