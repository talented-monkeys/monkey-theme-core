var gulpTask = function(){
	// Read about the gulp tasks on:
	// https://bitbucket.org/talented-monkeys/monkey-theme-core/src/[ENVIRONMENT]/build/gulp/core/
	// Replace [ENVIRONMENT] with: production, staging, development
	require('../../../content/themes/monkey-theme/build/gulp/gulpfile.build.javascript').registerTasks({
		namespace: 'parent',
		source: [
			'./content/themes/monkey-theme/assets/scripts'
		],
		dist: './content/themes/monkey-theme/dist/js',
		distTmp: './content/themes/monkey-theme/dist/.tmp',
		coffeeLintConfig: './config/tests/lint/coffee.lint.json',
		jsLintConfig: './config/tests/lint/js.lint.yml',
		tsLintConfig: './config/tests/lint/ts.lint.yml',
		notificationTimeOut: 5,
		includePaths: [
			'./content/themes/monkey-theme'
		]
	});
};


var GulpTaskJsParentTheme = function () {
    this.gulpTask = gulpTask;
};

module.exports = new GulpTaskJsParentTheme();
