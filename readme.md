
# Monkey Theme - Core

## Prerequisites
0. Install  `homebrew` for mac os users ([setup instructions](https://brew.sh/))
1. Grant setup scripts execution rights `chmod 777 ./setup/*`
2. Install node.js & npm `brew install node` ([instructions](https://blog.teamtreehouse.com/install-node-js-npm-mac)) or `sudo apt install nodejs npm` ([instructions](https://linuxize.com/post/how-to-install-node-js-on-ubuntu-18.04/))
3. Restart terminal and verify that **npm** is working `npm -v` 
4. Install composer for wordpress and dependency management
	1. Mac OS: `brew install composer` ([instructions](https://pilsniak.com/install-composer-mac-os/))
	2. Linux: `sudo apt-get install curl`  » `sudo curl -s https://getcomposer.org/installer | php`  » `sudo mv composer.phar /usr/local/bin/composer` ([instructions](https://www.ionos.com/community/hosting/php/install-and-use-php-composer-on-ubuntu-1604/))
5. Restart terminal and verify that **composer** if working `composer --version`
6. Setup APACHE/PHP/MySQL Environment
7. Create a MySQL user with the name `tm-build` and **without password**
8. Grant MySQL user `tm-build` all rights ([see attachment](https://bytebucket.org/talented-monkeys/monkey-theme-core/raw/45989d14d682975c4747d9ba3191c4214e754a89/setup/.instructions-files/mysql-user-tm-build-global-settings.jpg?token=1cdd661a3b5e3260b0458130b76dad8dcfe80a88))
9. Adding `.my.cnf` file without content using command `touch ~/.my.cnf`
10. Forking and Pullrequests

## Setup

0. Test
1. Create `./config/config.yml` based on  `./config/config.sample.yml`  
2. Define the configuration variables:
	- `environment` with you environment variable (eg: `dev`)
	- `debug` enable with `1` if you want to activate the debugging 
	- `project-name` with unique name for the project (eg: `monkey-theme-core`)
	- `project-path` with the absolute path to the root folder of the project (eg: `/var/www/monkey_theme_core`)
	- `project-type` with project type (eg: `internal` or `clients`)
	- `project-subtype` with project sub type (eg: `monkey-theme` or leave it empty `""`)
	- `project-port` with the in apache configured port for http (eg: `80`)
	- `project-ssl-https` should this project use https?  (eg: `enable` or `disable`)
	- `project-ssl-port` with the in apache configured port for http (eg: `443`)
	- `database-name` with your mysql database name (eg: `mt_core`)
	- `database-user` with your mysql database user (eg: `mt_core`)
	- `database-password` with your mysql database user password (eg: `password123`)
	- `database-host` with your mysql database host address (eg: `127.0.0.1`)
	- `smtp-user` with smtp user auth (eg: `username@domain.com`)
	- `smtp-pass` with smtp password auth (eg: `password123`)
	- `smtp-host` with smtp host (eg: `smtp.server.com`)
	- `smtp-from` with sender's email address (eg: `mailer@server.com`)
	- `smtp-name` with sender's name (eg: `Monkey Theme`)
	- `smtp-port` with smtp port (eg: `465` for ssl)
	- `smtp-secure` with smtp security type (eg: `ssl`)
	- `smtp-auth` if authentication is required of not (eg: `true`)
	- `wordpress-url` with used url for this project (eg: `monkey-theme.localhost`)
	- `wordpress-comment` this wordpress project allows user comments or not (eg: `enable` or `disable`)
	- `wordpress-blog` this wordpress project allows blog features or not (eg: `enable` or `disable`)
3. Run `npm install` which install `node_modules` and `dependencies`
4. Run `npm run setup` which will setup your project with all relevant `files`, `config` and `dependencies`. It will automatically run following setup `npm scripts` and `bash scripts`
	- `setup:project:build`:  create a project relevant `mysql database` and `user` and `database contents`
	- `setup:wordpress:build`: generates `wordpress core`, `monkey-theme & child`, `wp-config.php`
	- `setup:project:scripts`:  run project specific scripts (**written by you** - [see docs](https://bitbucket.org/talented-monkeys/monkey-theme-core/src/development/setup/project/))
	- `setup:after:scripts`:  adds `security processes` and `cleanup processes`


## Start a new project (out of this)

There are two options to create a new project out of this repository:

- Option 1 **[recommended]** is to [fork this repository](https://bitbucket.org/talented-monkeys/monkey-theme-core/fork).
- Option 2 is to clone and reconfigure the git remote url - see the following steps.

1. Go to [Bitbucket](https://bitbucket.org/repo/create) and create a new repository
2. Open your `terminal` and go to your webroot directory `cd /var/wwww`
3. Choose correct project parent folder (eg: `cd projects`)
4. Clone **this repo** with a new target folder (eg: `git clone git@bitbucket.org:talented-monkeys/monkey-theme-core.git <project-name-folder>`)
5. Move into new project folder `cd <project-name-folder>`
6. Set GIT remote url origin to new repository from step 1 `git remote set-url origin <new-repository-clone-url>`
7. Adapt the `project.md` file to fit your needs
8. Push into new remote origin `git push origin <branch-name>`

## File structure & overview
#### Sketchboard
Click on the image below to see the whole file strucure and get informations about each directory and file:
[![enter image description here](https://s3-eu-west-1.amazonaws.com/gallery-prod-4f50/img/567d88188ab548a89ed819926ce65fdb.png)](https://sketchboard.me/vBxupGwgzYpt#/)


## Wordpress Credentials

We defined a admin wordpress user with following credentials:

**IMPORTANT: Do not forget to create your own credentials and remove the monkey-theme user** 

- Username: `monkey-theme`
- Password: `nMUd(8uIzGn@WgEK4iMVW9po`