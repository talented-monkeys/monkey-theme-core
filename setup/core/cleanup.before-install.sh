#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml

if [ "$config_environment" == "local" ]
	then
		# Pre installation cleanup on local environment
		rm -rf ./composer.lock
		rm -rf ./package-lock.json
fi
# Pre installation cleanup
rm -rf ./wp/
rm -rf ./content/languages
rm -rf ./content/plugins
rm -rf ./content/themes/monkey-theme
rm -rf ./vendor/
rm -rf ./reports/
rm -rf ./wp-config.php
rm -rf ./gulpfile.js

rm -rf ./config/.htaccess
rm -rf ./data/.htaccess
rm -rf ./setup/.htaccess
rm -rf ./vendor/.htaccess