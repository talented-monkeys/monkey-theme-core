#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml

declare -a ALTERNATIVE_URLS=()

if [[ $config_project_url_alternatives_1st != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_1st}"); fi
if [[ $config_project_url_alternatives_2nd != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_2nd}"); fi
if [[ $config_project_url_alternatives_3rd != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_3rd}"); fi
if [[ $config_project_url_alternatives_4th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_4th}"); fi
if [[ $config_project_url_alternatives_5th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_5th}"); fi
if [[ $config_project_url_alternatives_6th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_6th}"); fi
if [[ $config_project_url_alternatives_7th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_7th}"); fi
if [[ $config_project_url_alternatives_8th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_8th}"); fi
if [[ $config_project_url_alternatives_9th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_9th}"); fi

echo "BUILD-PROJECT-REBUILD-SSL [START]"

if [ "$config_project_ssl_https" == "enable" ];
	then

		mkdir -p ${config_project_path}/config/ssl

		echo "BUILD-PROJECT-REBUILD-ALTERNATIVE-SSL [WIP] - Cool you're using https, let's add ssl encryption ..."
		SSL_CERTIFICATE_FILE=${config_project_path}/config/ssl/${config_environment}.${config_project_url_main}.crt
		SSL_PRIVATEKEY_FILE=${config_project_path}/config/ssl/${config_environment}.${config_project_url_main}.key

		touch $SSL_CERTIFICATE_FILE
		touch $SSL_PRIVATEKEY_FILE

		echo "BUILD-PROJECT-REBUILD-ALTERNATIVE-SSL [WIP] - Generating private.key and certificate file for project ..."
		# Following guidelines: https://letsencrypt.org/docs/certificates-for-localhost
		openssl req -x509 -out $SSL_CERTIFICATE_FILE -keyout $SSL_PRIVATEKEY_FILE -days 730 -newkey rsa:2048 -nodes -sha256 -subj "/CN=${config_project_url_main}" -extensions EXT -config <( printf "[dn]\nCN=${config_project_url_main}\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:${config_project_url_main}\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

		if [ "$(uname)" == "Darwin" ]; 
			then
				echo "BUILD-PROJECT-REBUILD-ALTERNATIVE-SSL [WIP] - Adding certificate to trusted certificates ..."
				security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain $SSL_CERTIFICATE_FILE
		else   
			echo "BUILD-PROJECT-REBUILD-SSL [WIP] - Please add to trusted certificates and set the config to 'trust always' ..."
			open $SSL_CERTIFICATE_FILE
		fi

		if [[ $config_project_url_alternatives_1st != "" ]]; 
			then
				echo "BUILD-PROJECT-REBUILD-SSL [WIP] - Hey look, there is an alternative url defined for this project ;)"
		elif [[ $config_project_url_alternatives_2nd != "" ]];
			then
				echo "BUILD-PROJECT-REBUILD-SSL [WIP] - Hey look, there are alternative urls defined for this project ;)"
		fi

		for ALTERNATIVE_URL in "${ALTERNATIVE_URLS[@]}"
			do
				echo "BUILD-PROJECT-REBUILD-ALTERNATIVE-SSL [WIP] - Cool you're using https, let's add ssl encryption ..."
				SSL_CERTIFICATE_FILE=${config_project_path}/config/ssl/${config_environment}.${ALTERNATIVE_URL}.crt
				SSL_PRIVATEKEY_FILE=${config_project_path}/config/ssl/${config_environment}.${ALTERNATIVE_URL}.key

				touch $SSL_CERTIFICATE_FILE
				touch $SSL_PRIVATEKEY_FILE

				echo "BUILD-PROJECT-REBUILD-ALTERNATIVE-SSL [WIP] - Generating private.key and certificate file for project ..."
				# Following guidelines: https://letsencrypt.org/docs/certificates-for-localhost
				openssl req -x509 -out $SSL_CERTIFICATE_FILE -keyout $SSL_PRIVATEKEY_FILE -days 730 -newkey rsa:2048 -nodes -sha256 -subj "/CN=${ALTERNATIVE_URL}" -extensions EXT -config <( printf "[dn]\nCN=${ALTERNATIVE_URL}\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:${ALTERNATIVE_URL}\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

				if [ "$(uname)" == "Darwin" ]; 
					then
						echo "BUILD-PROJECT-REBUILD-ALTERNATIVE-SSL [WIP] - Adding certificate to trusted certificates ..."
						security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain $SSL_CERTIFICATE_FILE
				else   
					echo "BUILD-PROJECT-REBUILD-SSL [WIP] - Please add to trusted certificates and set the config to 'trust always' ..."
					open $SSL_CERTIFICATE_FILE
				fi
		done

		echo "BUILD-PROJECT-REBUILD-SSL [WIP] - Restart apache to get virtual hosts config enabled"
		apachectl -k restart

else
	echo "BUILD-PROJECT-REBUILD-SSL [WARNING] - You are not using https - there is no need to rebuild your certificates (check your config.yml file)"
fi

echo "BUILD-PROJECT-REBUILD-SSL [DONE]"
