#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

echo "BUILD-PROJECT-GULP-TASKS [START]"

echo "BUILD-PROJECT-GULP-TASKS [PREPERATION] - Adding php build scripts to core project ..."
mkdir -p ./build/php
rm ./build/php/build.php2asset.php
cp ./content/themes/monkey-theme/build/php/build.php2asset.php ./build/php

echo "BUILD-PROJECT-GULP-TASKS [PREPERATION] - Adding lint config files to core project ..."
mkdir -p ./config/tests/lint
cp ./content/themes/monkey-theme/config/tests/lint/sass.lint.example.yml 			./config/tests/lint/sass.lint.example.yml
cp ./content/themes/monkey-theme/config/tests/lint/coffee.lint.example.json 	./config/tests/lint/coffee.lint.example.json
cp ./content/themes/monkey-theme/config/tests/lint/js.lint.example.yml 				./config/tests/lint/js.lint.example.yml

echo "BUILD-PROJECT-GULP-TASKS [WIP] - Generate gulpfile based on build gulp core tasks ..."
rm -rf ./gulpfile.js

BUILD_GULP_CORE_SCRIPT_PATH="./build/gulp/core/"

echo "// Monkey Theme Core - Gulp tasks" >> ./gulpfile.js

if [ "$(ls $BUILD_GULP_CORE_SCRIPT_PATH)" ]; then
 	for GULP_CORE_SCRIPT in $(ls -ar $BUILD_GULP_CORE_SCRIPT_PATH*)
	 	do
		 	if [[ $GULP_CORE_SCRIPT == *.js ]];
		 		then
					echo "BUILD-PROJECT-GULP-TASKS [WIP] - Adding '${GULP_CORE_SCRIPT}' to the gulpfile ..."
					echo "require('${GULP_CORE_SCRIPT}').gulpTask();" >> ./gulpfile.js
		 	fi
		done
else
		echo "BUILD-PROJECT-GULP-TASKS [WARNING] - No gulp core tasks to add ..."
fi

echo " " >> ./gulpfile.js

echo "BUILD-PROJECT-GULP-TASKS [WIP] - Generate gulpfile based on build gulp project related tasks ..."

BUILD_GULP_PROJECT_SCRIPT_PATH="./build/gulp/project/"

if [ "$(ls -ar $BUILD_GULP_PROJECT_SCRIPT_PATH)" ]; then
	echo "// Project related - Gulp tasks" >> ./gulpfile.js

 	for GULP_PROJECT_SCRIPT in $(ls -ar $BUILD_GULP_PROJECT_SCRIPT_PATH*)
	do
		if [[ $GULP_PROJECT_SCRIPT == *.js ]];
			then
				echo "BUILD-PROJECT-GULP-TASKS [WIP] - Adding '${GULP_PROJECT_SCRIPT}' to the gulpfile ..."
			  echo "require('${GULP_PROJECT_SCRIPT}').gulpTask();" >> ./gulpfile.js
		fi
	done
else
		echo "BUILD-PROJECT-GULP-TASKS [WARNING] - No project related gulp tasks to add ..."
fi

echo "BUILD-PROJECT-GULP-TASKS [END]"
