#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

echo "BUILD-PROJECT-BUILD-ASSETS [START]"

echo "BUILD-PROJECT-BUILD-ASSETS [WIP] - Install bower components on monkey-theme ..."
(cd ./content/themes/monkey-theme && npm install)

echo "BUILD-PROJECT-BUILD-ASSETS [WIP] - Install bower components on monkey-theme child ..."
(cd ./content/themes/monkey-theme-child && npm install)

echo "BUILD-PROJECT-BUILD-ASSETS [WIP] - Build assets from parent & child with gulp ..."
npm run gulp parent:assets:build && npm run gulp child:assets:build

echo "BUILD-PROJECT-BUILD-ASSETS [END]"
