#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml


VHOSTS_DIRECTORY=/etc/apache2/vhosts

echo "BUILD-PROJECT-APACHE-SETUP [START]"

if [ "$config_environment" == "local" ]
	then
		echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Remove url (${config_project_url_main}) from hosts file if already exists ..."
		sed -i -e "s|127.0.0.1 ${config_project_url_main}||g" /etc/hosts

		echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Adding url (${config_project_url_main}) to hosts file ..."
		rm -rf ./setup/.tmp.hosts
		touch ./setup/.tmp.hosts
		cat -s /etc/hosts > ./setup/.tmp.hosts
		echo "127.0.0.1 ${config_project_url_main}" >> ./setup/.tmp.hosts
		cat ./setup/.tmp.hosts > /etc/hosts
		rm -rf ./setup/.tmp.hosts

		echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Remove virtual hosts file for (${config_project_url_main}) from vhosts if already exists ..."
		rm -rf /etc/apache2/vhosts/${config_project_type}/${config_project_name}/${config_project_name}.conf

		echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Generate virtual hosts file for (${config_project_url_main}) ..."
		mkdir -p ./setup/templates/${config_project_name}

		if [ "$config_project_ssl_https" == "enable" ]
			then
				echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Cool you're using https, let's add ssl encryption ..."
				SSL_CERTIFICATE_FILE=${config_project_path}/config/ssl/${config_environment}.${config_project_url_main}.crt
				SSL_PRIVATEKEY_FILE=${config_project_path}/config/ssl/${config_environment}.${config_project_url_main}.key

				mkdir -p ${config_project_path}/config/ssl

				touch $SSL_CERTIFICATE_FILE
				touch $SSL_PRIVATEKEY_FILE

				cp ./setup/templates/core.template.dev.virtual-hosts-ssl.conf ./setup/templates/${config_project_name}/${config_project_name}.conf
				sed -i"" -e  "s|REPLACE_PROJECT_SSL_PORT|${config_project_ssl_port}|g" ./setup/templates/${config_project_name}/${config_project_name}.conf
				sed -i"" -e  "s|REPLACE_PROJECT_SSL_CER_FILE|${SSL_CERTIFICATE_FILE}|g" ./setup/templates/${config_project_name}/${config_project_name}.conf
				sed -i"" -e  "s|REPLACE_PROJECT_SSL_KEY_FILE|${SSL_PRIVATEKEY_FILE}|g" ./setup/templates/${config_project_name}/${config_project_name}.conf

				echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Generating private.key and certificate file for project ..."
				# Following guidelines: https://letsencrypt.org/docs/certificates-for-localhost
				openssl req -x509 -out $SSL_CERTIFICATE_FILE -keyout $SSL_PRIVATEKEY_FILE -days 730 -newkey rsa:2048 -nodes -sha256 -subj "/CN=${config_project_url_main}" -extensions EXT -config <( printf "[dn]\nCN=${config_project_url_main}\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:${config_project_url_main}\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

				if [ "$(uname)" == "Darwin" ];
					then
						echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Adding certificate to trusted certificates ..."
						security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain $SSL_CERTIFICATE_FILE
    		else
					echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Please add to trusted certificates and set the config to 'trust always' ..."
					open $SSL_CERTIFICATE_FILE
				fi
		else
			cp ./setup/templates/core.template.dev.virtual-hosts-default.conf ./setup/templates/${config_project_name}/${config_project_name}.conf

		fi
		sed -i"" -e  "s|REPLACE_PROJECT_PORT|${config_project_port}|g" ./setup/templates/${config_project_name}/${config_project_name}.conf
		sed -i"" -e  "s|REPLACE_PROJECT_URL|${config_project_url_main}|g" ./setup/templates/${config_project_name}/${config_project_name}.conf
		sed -i"" -e  "s|REPLACE_PROJECT_PATH|${config_project_path}|g" ./setup/templates/${config_project_name}/${config_project_name}.conf

		echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Move virtual hosts file for (${config_project_url_main}) to '/etc/apache2/vhosts/${config_project_type}/${config_project_name}/${config_project_name}.conf' ..."

		if [[ ! -e "${VHOSTS_DIRECTORY}" ]]; then
			mkdir -p ${VHOSTS_DIRECTORY}
		fi
		if [[ ! -e "${VHOSTS_DIRECTORY}/${config_project_type}" ]]; then
			mkdir -p ${VHOSTS_DIRECTORY}/${config_project_type}
		fi
		if [[ ! -e "${VHOSTS_DIRECTORY}/${config_project_type}/${config_project_name}/" ]]; then
			mkdir -p ${VHOSTS_DIRECTORY}/${config_project_type}/${config_project_name}
		fi

		mv ./setup/templates/${config_project_name}/${config_project_name}.conf /etc/apache2/vhosts/${config_project_type}/${config_project_name}
		rm -rf ./setup/templates/${config_project_name}

		echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Remove virtual hosts config for (${config_project_url_main}) from vhosts file if already exists ..."
		sed -i -e "s|# Adding virtual hosts config for url ${config_project_url_main}||g" /etc/apache2/httpd.conf
		sed -i -e "s|Include /etc/apache2/vhosts/${config_project_type}/${config_project_name}/${config_project_name}.conf||g" /etc/apache2/httpd.conf

		echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Adding virtual hosts config for (${config_project_url_main}) to vhosts file ..."
		rm -rf ./setup/.tmp.httpd.conf
		touch ./setup/.tmp.httpd.conf
		cat -s /etc/apache2/httpd.conf > ./setup/.tmp.httpd.conf
		echo "# Adding virtual hosts config for url ${config_project_url_main}" >> ./setup/.tmp.httpd.conf
		echo "Include /etc/apache2/vhosts/${config_project_type}/${config_project_name}/${config_project_name}.conf" >> ./setup/.tmp.httpd.conf
		cat ./setup/.tmp.httpd.conf > /etc/apache2/httpd.conf
		rm -rf ./setup/.tmp.httpd.conf

		if [[ $config_project_url_alternatives_1st != "" ]];
			then
				echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Hey look, there are alternative urls defined for this project ;)"
				./setup/core/project.apache-alternative-urls-build.sh
			else
				echo "BUILD-PROJECT-APACHE-SETUP [WIP] - Restart apache to get virtual hosts config enabled"
				apachectl -k restart
		fi
fi

echo "BUILD-PROJECT-APACHE-SETUP [DONE]"
