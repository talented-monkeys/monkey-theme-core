#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml

declare -a ALTERNATIVE_URLS=()

if [[ $config_project_url_alternatives_1st != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_1st}"); fi
if [[ $config_project_url_alternatives_2nd != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_2nd}"); fi
if [[ $config_project_url_alternatives_3rd != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_3rd}"); fi
if [[ $config_project_url_alternatives_4th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_4th}"); fi
if [[ $config_project_url_alternatives_5th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_5th}"); fi
if [[ $config_project_url_alternatives_6th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_6th}"); fi
if [[ $config_project_url_alternatives_7th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_7th}"); fi
if [[ $config_project_url_alternatives_8th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_8th}"); fi
if [[ $config_project_url_alternatives_9th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_9th}"); fi

VHOSTS_DIRECTORY=/etc/apache2/vhosts
MAIN_VHOST_FILE=/etc/apache2/vhosts/${config_project_type}/${config_project_name}/${config_project_name}.conf

echo "BUILD-PROJECT-ALTERNATIVE-URL [START]"
mkdir -p ./setup/templates/${config_project_name}

for ALTERNATIVE_URL in "${ALTERNATIVE_URLS[@]}"
	do
		echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Remove url (${ALTERNATIVE_URL}) from hosts file if already exists ..."
		sed -i -e "s|127.0.0.1 ${ALTERNATIVE_URL}||g" /etc/hosts

		echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Adding url (${ALTERNATIVE_URL}) to hosts file ..."
		rm -rf ./setup/.tmp.hosts
		touch ./setup/.tmp.hosts
		cat -s /etc/hosts > ./setup/.tmp.hosts
		echo "127.0.0.1 ${ALTERNATIVE_URL}" >> ./setup/.tmp.hosts
		cat ./setup/.tmp.hosts > /etc/hosts
		rm -rf ./setup/.tmp.hosts

		echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Generate virtual hosts file for (${ALTERNATIVE_URL}) ..."
		if [ "$config_project_ssl_https" == "enable" ]
			then
				echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Cool you're using https, let's add ssl encryption ..."
				SSL_CERTIFICATE_FILE=${config_project_path}/config/ssl/${config_environment}.${ALTERNATIVE_URL}.crt
				SSL_PRIVATEKEY_FILE=${config_project_path}/config/ssl/${config_environment}.${ALTERNATIVE_URL}.key

				mkdir -p ${config_project_path}/config/ssl

				touch $SSL_CERTIFICATE_FILE
				touch $SSL_PRIVATEKEY_FILE

				cp ./setup/templates/core.template.dev.virtual-hosts-ssl.conf ./setup/templates/${config_project_name}/${ALTERNATIVE_URL}.conf
				sed -i"" -e  "s|REPLACE_PROJECT_SSL_PORT|${config_project_ssl_port}|g" ./setup/templates/${config_project_name}/${ALTERNATIVE_URL}.conf
				sed -i"" -e  "s|REPLACE_PROJECT_SSL_CER_FILE|${SSL_CERTIFICATE_FILE}|g" ./setup/templates/${config_project_name}/${ALTERNATIVE_URL}.conf
				sed -i"" -e  "s|REPLACE_PROJECT_SSL_KEY_FILE|${SSL_PRIVATEKEY_FILE}|g" ./setup/templates/${config_project_name}/${ALTERNATIVE_URL}.conf

				echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Generating private.key and certificate file for project ..."
				# Following guidelines: https://letsencrypt.org/docs/certificates-for-localhost
				openssl req -x509 -out $SSL_CERTIFICATE_FILE -keyout $SSL_PRIVATEKEY_FILE -days 730 -newkey rsa:2048 -nodes -sha256 -subj "/CN=${ALTERNATIVE_URL}" -extensions EXT -config <( printf "[dn]\nCN=${ALTERNATIVE_URL}\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:${ALTERNATIVE_URL}\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

				if [ "$(uname)" == "Darwin" ]; 
					then
						echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Adding certificate to trusted certificates ..."
						security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain $SSL_CERTIFICATE_FILE
    		else   
					echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Please add to trusted certificates and set the config to 'trust always' ..."
					open $SSL_CERTIFICATE_FILE
				fi
		else
			cp ./setup/templates/core.template.dev.virtual-hosts-default.conf ./setup/templates/${config_project_name}/${ALTERNATIVE_URL}.conf

		fi
		sed -i"" -e  "s|REPLACE_PROJECT_PORT|${config_project_port}|g" ./setup/templates/${config_project_name}/${ALTERNATIVE_URL}.conf
		sed -i"" -e  "s|REPLACE_PROJECT_URL|${ALTERNATIVE_URL}|g" ./setup/templates/${config_project_name}/${ALTERNATIVE_URL}.conf
		sed -i"" -e  "s|REPLACE_PROJECT_PATH|${config_project_path}|g" ./setup/templates/${config_project_name}/${ALTERNATIVE_URL}.conf

		mv ./setup/templates/${config_project_name}/${ALTERNATIVE_URL}.conf /etc/apache2/vhosts/${config_project_type}/${config_project_name}

		echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Remove virtual hosts config for (${ALTERNATIVE_URL}) from vhosts file if already exists ..."
		sed -i -e "s|# Adding virtual hosts config for url ${ALTERNATIVE_URL}||g" $MAIN_VHOST_FILE
		sed -i -e "s|Include /etc/apache2/vhosts/${config_project_type}/${config_project_name}/${ALTERNATIVE_URL}.conf||g" $MAIN_VHOST_FILE

		echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Adding virtual hosts config for (${ALTERNATIVE_URL}) to vhosts file ..."
		echo "# Adding virtual hosts config for url ${ALTERNATIVE_URL}" >> $MAIN_VHOST_FILE
		echo "Include /etc/apache2/vhosts/${config_project_type}/${config_project_name}/${ALTERNATIVE_URL}.conf" >> $MAIN_VHOST_FILE
done

rm -rf ./setup/templates/${config_project_name}

echo "BUILD-PROJECT-ALTERNATIVE-URL [WIP] - Restart apache to get virtual hosts config enabled"
apachectl -k restart

echo "BUILD-PROJECT-ALTERNATIVE-URL [DONE]"