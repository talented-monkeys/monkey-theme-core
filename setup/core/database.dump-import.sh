#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml

# create random password
DBNAME="$config_database_name"
DBUSER="$config_database_user"
DBPASS="$config_database_password"
DBHOST="$config_database_host"

PROJECT_ENV="$config_environment"
PROJECT_NAME="$config_project_name"
PROJECT_URL="$config_project_url_main"

DBDUMP_PATH="./data/db/"
DBDUMP_NAME="${PROJECT_ENV}.${PROJECT_NAME}.sql"
DBDUMP="${DBDUMP_PATH}${DBDUMP_NAME}"

DATABASE_DEFAULT_PROJECT_URL=monkey-theme.localhost

if [ -f "${DBDUMP}.gz" ]; then
   DBDUMP_FILE=$DBDUMP
else
   DBDUMP_FILE=$DBDUMP_PATH$PROJECT_ENV.dump.sql
fi

echo "SETUP-DATABASE-IMPORT [START]"

echo "SETUP-DATABASE-IMPORT [WIP] - Start import project database dump to database ($DBNAME) ..."

gunzip -k "${DBDUMP_FILE}.gz"

if grep -q $PROJECT_URL ${DBDUMP_FILE}; then
	if [ "$config_project_ssl_https" == "enable" ]
		then
			sed -i"" -e  "s|http://${PROJECT_URL}|https://${PROJECT_URL}|g" ${DBDUMP_FILE}
	else
		sed -i"" -e  "s|https://${PROJECT_URL}|http://${PROJECT_URL}|g" ${DBDUMP_FILE}
	fi
	echo "SETUP-DATABASE-IMPORT [WIP] - Import running ..."
	mysql --host="${DBHOST}" --user="${DBUSER}" --password="${DBPASS}" $DBNAME < ${DBDUMP_FILE}
else
	if grep -q $DATABASE_DEFAULT_PROJECT_URL ${DBDUMP_FILE}; then
		echo "SETUP-DATABASE-IMPORT [WIP] - Using default database dump, we need to replace the project url (${PROJECT_URL}) ..."
		sed -i"" -e  "s|${DATABASE_DEFAULT_PROJECT_URL}|${PROJECT_URL}|g" ${DBDUMP_FILE}
		if [ "$config_project_ssl_https" == "enable" ]
			then
				sed -i"" -e  "s|http://${PROJECT_URL}|https://${PROJECT_URL}|g" ${DBDUMP_FILE}
		else
			sed -i"" -e  "s|https://${PROJECT_URL}|http://${PROJECT_URL}|g" ${DBDUMP_FILE}
		fi
		echo "SETUP-DATABASE-IMPORT [WIP] - Import running ..."
		mysql --host="${DBHOST}" --user="${DBUSER}" --password="${DBPASS}" $DBNAME < ${DBDUMP_FILE}
		echo "SETUP-DATABASE-IMPORT [WIP] - After we successfully changed the project url (${PROJECT_URL}) in the database dump, we need to create a new one ..."
		npm run setup:database:export
		echo "SETUP-DATABASE-IMPORT [WIP] - A new database dump with url (${PROJECT_URL}) was created, please commit the new database dump to the repository ..."
	else
		echo "SETUP-DATABASE-IMPORT [ERROR] - ${PROJECT_URL} was not found in mysql dump!"
	fi
fi

rm -rf $DBDUMP_FILE

echo "SETUP-DATABASE-IMPORT [DONE]"
