#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

rm ./wp-config.php

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh


# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml

# copy wp-config file from wp-config.sample
cp ./config/wp-config.sample.php ./config/wp-config.php


echo 'WORDPRESS-GENERATE-CONFIG [START] - Set configuration of wp-config.php as "'$config_environment'"-setup like defined in config.yml'

# Replace DB_USER in wp-config.php
sed -i"" -e "s/REPLACE_DB_USER/${config_database_user}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - MySQL - Replaced db-user with "'$config_database_user'" in wp-config.php file ...'

# Replace DB_PASSWORD in wp-config.php
sed -i"" -e "s/REPLACE_DB_PASSWORD/${config_database_password}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - MySQL - Replaced db-password with "••••••••••••••••" in wp-config.php file ...'

# Replace DB_NAME in wp-config.php
sed -i"" -e "s/REPLACE_DB_NAME/${config_database_name}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - MySQL - Replaced db-name with "'$config_database_name'" in wp-config.php file ...'

# Replace DB_HOST in wp-config.php
sed -i"" -e "s/REPLACE_DB_HOST/${config_database_host}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - MySQL - Replaced db-host with "'$config_database_host'" in wp-config.php file ...'

# Replace SMTP_YYYY in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_YYYY/${config_smtp_xxx}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp xxx with "'$config_smtp_xxx'" in wp-config.php file ...'

# Replace SMTP_USER in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_USER/${config_smtp_user}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp user with "'$config_smtp_user'" in wp-config.php file ...'

# Replace SMTP_PASS in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_PASS/${config_smtp_password}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp password with "••••••••••••••••" in wp-config.php file ...'

# Replace SMTP_HOST in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_HOST/${config_smtp_host}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp host with "'$config_smtp_host'" in wp-config.php file ...'

# Replace SMTP_FROM in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_FROM/${config_smtp_from}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp from with "'$config_smtp_from'" in wp-config.php file ...'

# Replace SMTP_NAME in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_NAME/${config_smtp_name}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp name with "'$config_smtp_name'" in wp-config.php file ...'

# Replace SMTP_PORT in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_PORT/${config_smtp_port}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp port with "'$config_smtp_port'" in wp-config.php file ...'

# Replace SMTP_SECURE in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_SECURE/${config_smtp_secure}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp secure with "'$config_smtp_secure'" in wp-config.php file ...'

# Replace SMTP_AUTH in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_AUTH/${config_smtp_auth}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp auth with "'$config_smtp_auth'" in wp-config.php file ...'

# Replace SMTP_DEBUG in wp-config.php
sed -i"" -e "s/REPLACE_SMTP_DEBUG/${config_debug}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SMTP - Replaced smtp debug with "'$config_debug'" in wp-config.php file ...'

# Replace WORDPRESS_URL in wp-config.php
if [ "$config_project_ssl_https" == "enable" ]
	then
		sed -i"" -e "s|REPLACE_WORDPRESS_URL|https://${config_project_url_main}|g" ./config/wp-config.php
else
	sed -i"" -e "s|REPLACE_WORDPRESS_URL|http://${config_project_url_main}|g" ./config/wp-config.php
fi
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - URL - Replaced wordpress url with "'$config_project_url_main'" in wp-config.php file ...'

# Activate FORCE_SSL_LOGIN & FORCE_SSL_ADMIN in wp-config.php
if [ "$config_project_ssl_https" == "enable" ]
	then
		sed -i"" -e "s/'FORCE_SSL_LOGIN', false/'FORCE_SSL_LOGIN', true/g" ./config/wp-config.php
		sed -i"" -e "s/'FORCE_SSL_ADMIN', false/'FORCE_SSL_ADMIN', true/g" ./config/wp-config.php
		echo 'WORDPRESS-GENERATE-CONFIG [WIP] - SSL - Force admin to use ssl fot "https://'$config_project_url_main'" in wp-config.php file ...'
fi

# Replace PROJECT_ENVIRONMENT in wp-config.php
sed -i"" -e "s/REPLACE_PROJECT_ENVIRONMENT/${config_environment}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - PROJECT - Replaced project environment with "'$config_environment'" in wp-config.php file ...'

# Replace WP_COMMENTS_ALLOWED in wp-config.php
sed -i"" -e "s/REPLACE_WP_COMMENTS/${config_wordpress_comments}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - COMMENTS - Replaced wordpress comments with "'$config_wordpress_comments'" in wp-config.php file ...'

# Replace WP_BLOG_ALLOWED in wp-config.php
sed -i"" -e "s/REPLACE_WP_BLOG/${config_wordpress_blog}/g" ./config/wp-config.php
echo 'WORDPRESS-GENERATE-CONFIG [WIP] - BLOG - Replaced wordpress blog with "'$config_wordpress_blog'" in wp-config.php file ...'

mv ./config/wp-config.php ./

echo 'WORDPRESS-GENERATE-CONFIG [DONE]'
