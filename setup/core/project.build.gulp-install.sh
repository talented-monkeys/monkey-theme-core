#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

echo "BUILD-PROJECT-GULP-INSTALL [START]"

echo "BUILD-PROJECT-GULP-INSTALL [WIP] - Symlink node_modules to core project ..."
rm -rf ./content/themes/monkey-theme/node_modules
rm -rf ./node_modules
ln -s ./content/themes/monkey-theme/node_modules ./node_modules

echo "BUILD-PROJECT-GULP-INSTALL [WIP] - Install npm dependencies (npm install) on monkey-theme ..."
(cd ./content/themes/monkey-theme && npm install)

echo "BUILD-PROJECT-GULP-INSTALL [END]"