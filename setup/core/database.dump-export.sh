#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml

# create random password
DATABASE="$config_database_name"
USER="$config_database_user"
PASSWORD="$config_database_password"
HOST="$config_database_host"
PROJECT_ENV="$config_environment"
PROJECT_NAME="$config_project_name"

# Backup variables: Output config
OUTPUTPATH="./data/db/"

if [[ $1 == initial ]]; then
	OUTPUTNAME="$PROJECT_ENV.dump.sql.gz"
elif [[ $1 == dev ]]; then
	OUTPUTNAME_MYSQL="dev.$PROJECT_NAME.sql"
	OUTPUTNAME="dev.$PROJECT_NAME.sql.gz"
else
	OUTPUTNAME="$PROJECT_ENV.$PROJECT_NAME.sql.gz"
fi

OUTPUT=$OUTPUTPATH$OUTPUTNAME

# Remove old db snapshots
rm -rf $OUTPUT
mkdir -p $OUTPUTPATH


echo "SETUP-DATABASE-EXPORT [START]"

echo "SETUP-DATABASE-EXPORT [WIP] - Creating database dump '$OUTPUT' from '$DATABASE' as user '$USER' on server '$HOST' ..."

if [[ $1 == dev ]]; then
	mysqldump --host="${HOST}" --user="${USER}" --password="${PASSWORD}" $DATABASE > $OUTPUTPATH$OUTPUTNAME_MYSQL
	sed -i"" -e  "s|//local.|//dev.|g" $OUTPUTPATH$OUTPUTNAME_MYSQL
	gzip -9 -k $OUTPUTPATH$OUTPUTNAME_MYSQL
	rm -rf $OUTPUTPATH$OUTPUTNAME_MYSQL
	rm -rf $OUTPUTPATH$OUTPUTNAME_MYSQL-e
else
	# Backup command
	mysqldump --host="${HOST}" --user="${USER}" --password="${PASSWORD}" $DATABASE | gzip -9 > $OUTPUT
fi

echo "SETUP-DATABASE-EXPORT [DONE]"
