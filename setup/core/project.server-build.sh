#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml

echo "BUILD-PROJECT-SERVER-SETUP [START]"


echo $config_server_type

if [ "$config_server_type" == "apache" ]
	then
		echo "BUILD-PROJECT-SERVER-SETUP [WIP] - Apache server type chosed - lets build the server configuration..."
		./setup/core/project.apache-build.sh
fi

if [ "$config_server_type" == "valet" ]
	then
		echo "BUILD-PROJECT-SERVER-SETUP [WIP] -Valet server type chosed - thats nice, less work for me ;)"
		./setup/core/project.valet-build.sh
fi

echo "BUILD-PROJECT-SERVER-SETUP [DONE]"
