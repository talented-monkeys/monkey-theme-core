#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml


VHOSTS_DIRECTORY=/etc/apache2/vhosts

echo "BUILD-PROJECT-VALET-SETUP [START]"

if [ "$config_environment" == "local" ]
	then
		echo "BUILD-PROJECT-VALET-SETUP [WIP] - Adding project to valet project with 'valet park'"
		cd ${config_project_path} && cd .. && valet park

		if [ "$config_project_ssl_https" == "enable" ]
			then
				echo "BUILD-PROJECT-VALET-SETUP [WIP] - Hey look, https is enabled, let us secure this project with an ssl certificate ;)"
				cd ${config_project_path} && valet secure
		fi

		if [[ $config_project_url_alternatives_1st != "" ]];
			then
				echo "BUILD-PROJECT-VALET-SETUP [WIP] - Hey look, there are alternative urls defined for this project ;)"

				declare -a ALTERNATIVE_URLS=()

				if [[ $config_project_url_alternatives_1st != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_1st}"); fi
				if [[ $config_project_url_alternatives_2nd != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_2nd}"); fi
				if [[ $config_project_url_alternatives_3rd != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_3rd}"); fi
				if [[ $config_project_url_alternatives_4th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_4th}"); fi
				if [[ $config_project_url_alternatives_5th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_5th}"); fi
				if [[ $config_project_url_alternatives_6th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_6th}"); fi
				if [[ $config_project_url_alternatives_7th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_7th}"); fi
				if [[ $config_project_url_alternatives_8th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_8th}"); fi
				if [[ $config_project_url_alternatives_9th != "" ]]; then ALTERNATIVE_URLS+=("${config_project_url_alternatives_9th}"); fi

				for ALTERNATIVE_URL in "${ALTERNATIVE_URLS[@]}"
					do
						echo "BUILD-PROJECT-VALET-SETUP [WIP] - Adding ${ALTERNATIVE_URL} to the valet configuration..."

						CURRENT_URL=${ALTERNATIVE_URL/".test"/""}

						cd ${config_project_path} && valet link ${CURRENT_URL}

						if [ "$config_project_ssl_https" == "enable" ]
							then
								echo "BUILD-PROJECT-VALET-SETUP [WIP] - Hey look, https is enabled, let us secure ${ALTERNATIVE_URL} with an ssl certificate ;)"
								cd ${config_project_path} && valet secure ${CURRENT_URL}
						fi

				done
		fi
fi

echo "BUILD-PROJECT-VALET-SETUP [DONE]"
