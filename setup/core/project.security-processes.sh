#!/bin/bash


# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

rm -rf ./config/.htaccess
rm -rf ./data/.htaccess
rm -rf ./setup/.htaccess
rm -rf ./vendor/.htaccess
rm -rf ./build/.htaccess

touch ./config/.htaccess

echo "Order allow,deny" >> ./config/.htaccess
echo "Deny from all" >> ./config/.htaccess

cp ./config/.htaccess ./data/.htaccess
cp ./config/.htaccess ./setup/.htaccess
cp ./config/.htaccess ./vendor/.htaccess
cp ./config/.htaccess ./build/.htaccess