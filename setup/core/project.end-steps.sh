#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml

if [ "$config_project_ssl_https" == "enable" ]
	then
		PROJECT_URL="https://${config_project_url_main}"
else
		PROJECT_URL="http://${config_project_url_main}"
fi

echo "ENDING-PROJECT-SETUP [START]"

echo "ENDING-PROJECT-SETUP [WIP] - Currently nothing to do ..."

if [ "$config_environment" == "local" ]
	then
		echo "ENDING-PROJECT-SETUP [END] - Let us start working, open your webpage (${PROJECT_URL})"
		open $PROJECT_URL
	else
		echo "ENDING-PROJECT-SETUP [END]"
fi
