#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

WP_CHILD_THEME_DIRECTORY="./content/themes/monkey-theme-child"


echo "GENERATE-WORDPRESS-CHILD-THEME [START]"

if [[ ! -e "${WP_CHILD_THEME_DIRECTORY}" ]]; then
	echo "GENERATE-WORDPRESS-CHILD-THEME [WIP] - clone child theme repository ..."
 	git clone git@bitbucket.org:talented-monkeys/monkey-theme-child.git ./content/themes/monkey-theme-child

	echo "GENERATE-WORDPRESS-CHILD-THEME [WIP] - remove git settings ..."
	rm -rf ./content/themes/monkey-theme-child/.git
else
	echo "GENERATE-WORDPRESS-CHILD-THEME [INFO] - Child theme already there / no need to generate"
fi

echo "ADDING WORDPRESS-CHILD-THEME COMPOSER PACKAGES [START]"

(cd ./content/themes/monkey-theme-child/ && composer install)

echo "ADDING WORDPRESS-CHILD-THEME COMPOSER PACKAGES [END]"

echo "GENERATE-WORDPRESS-CHILD-THEME [END]"
