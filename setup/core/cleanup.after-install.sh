#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# Wordpress cleanup
rm -rf ./wp/wp-content/plugins
mkdir -p ./wp/wp-content/plugins
touch ./wp/wp-content/plugins/index.php && echo "<?php" > ./wp/wp-content/plugins/index.php

rm -rf ./wp/wp-content/themes
mkdir -p ./wp/wp-content/themes
touch ./wp/wp-content/themes/index.php && echo "<?php" > ./wp/wp-content/themes/index.php