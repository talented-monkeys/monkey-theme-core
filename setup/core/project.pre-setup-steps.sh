#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

echo "INITIAL-PROJECT-SETUP [START]"

echo "INITIAL-PROJECT-SETUP [WIP] - Generate project related files ..."

echo "INITIAL-PROJECT-SETUP [WIP] - Copied individual project scripts file - please rename if needed"
cp ./setup/templates/core.example.setup.project.scripts.sh ./setup/project/scripts.sh

echo "INITIAL-PROJECT-SETUP [END]"