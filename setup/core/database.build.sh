#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

# include parse_yaml function
. ./setup/share/core.tools.parse-yaml.sh

# read yaml file
sed '/^[[:blank:]]*#/d;s/#.*//' ./config/config.yml > ./config/.tmp.config.yml
eval $(parse_yaml ./config/.tmp.config.yml "config_")
rm -rf ./config/.tmp.config.yml

# create random password
DBNAME="${config_database_name}"
DBUSER="${config_database_user}"
DBPASS="${config_database_password}"
DBHOST="${config_database_host}"

PROJECT_ENV="${config_environment}"

echo "SETUP-DATABASE [START]"

RESULT=`mysqlshow --user=${DBUSER} --password=${DBPASS} --host=${DBHOST} ${DBNAME}| grep -v Wildcard | grep -o ${DBNAME}`
if [ "$RESULT" == "${DBNAME}" ];
	then
    	echo "SETUP-DATABASE [WIP] - Work already done, ${DBNAME} already exists and ${DBUSER} is assigned to it..."
	else
		# If  ~/.my.cnf exists then it won't ask for tm-build password
		if [ -f ~/.my.cnf ]; then

		    echo "SETUP-DATABASE [WIP] - Create new database: ${DBNAME} ..."
		    mysql -u tm-build -e "CREATE DATABASE \`${DBNAME}\` CHARACTER SET utf8 COLLATE utf8_general_ci;"

		    echo "SETUP-DATABASE [WIP] - Create new user: ${DBUSER} with password '••••••••••••••••' ..."
		    mysql -u tm-build -e "CREATE USER '${DBUSER}'@${DBHOST} IDENTIFIED BY '${DBPASS}';"

		    echo "SETUP-DATABASE [WIP] - Assign user ($DBUSER) to database ($DBNAME) ..."
		    mysql -u tm-build -e "GRANT ALL PRIVILEGES ON \`${DBNAME}\`.* TO '${DBUSER}'@'${DBHOST}';"
		    mysql -u tm-build -e "FLUSH PRIVILEGES;"

		# If  ~/.my.cnf doesn't exist then it'll ask for root password
		else
		    echo "Please enter root user MySQL password!"
		    read -sp rootpasswd
		    echo "SETUP-DATABASE [WIP] - Create new database: ${DBNAME} ..."
		    mysql -u root -p${rootpasswd} -e "CREATE DATABASE \`${DBNAME}\` CHARACTER SET utf8 COLLATE utf8_general_ci;"

		    echo "SETUP-DATABASE [WIP] - Create new user: ${DBUSER} ..."
		    mysql -u root -p${rootpasswd} -e "CREATE USER '${DBUSER}'@${DBHOST} IDENTIFIED BY '${DBPASS}';"

		    echo "SETUP-DATABASE [WIP] - Assign user ($DBUSER) to database ($DBNAME) ..."
		    mysql -u root -p${rootpasswd} -e "GRANT ALL PRIVILEGES ON \`${DBNAME}\`.* TO '${DBUSER}'@'${DBHOST}';"
		    mysql -u root -p${rootpasswd} -e "FLUSH PRIVILEGES;"
		fi
fi

echo "SETUP-DATABASE [DONE]"
