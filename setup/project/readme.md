# Project Setup

Please add your project specific setup files to this folder.
It is important that you add them like written in that example, because that will avoid conflicts with the forked repo updates.

The scripts will automatically run within the initial setup step `npm run setup` and on every `npm run setup:project:scripts` command.

## How to add custom scripts to your project

**1)**  Rename the generated `./setup/project/scripts.sh.example` into  `./setup/project/scripts.sh`

**2)**  Give it execution right  `chmod 777 ./setup/project/scripts.sh`

**3)** Create a new `bash file` into the directory `./setup/project/<fancy-script-name.sh>`

**4)** Give it execution right `chmod 777 ./setup/project/<fancy-script-name.sh>`

**5)** Write down your bash code for the created file and :

```bash
#!/bin/bash
# add your bash code here
```

**5)**  Add your script to the scripts file `./setup/project/scripts.sh`

```bash
#!/bin/bash

# Give execution rights to setup files
chmod 777 ./setup/core/*
chmod 777 ./setup/project/*
chmod 777 ./setup/share/*

echo "PROJECTS-SCRIPTS [START]"

# START: Add here to add your sh scripts
echo "PROJECTS-SCRIPTS [WIP] - <Explain what your script is doing>"
./setup/project/<fancy-script-name.sh>

# END

echo "PROJECTS-SCRIPTS [START]"
```
